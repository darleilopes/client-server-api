package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	db, err := sql.Open("sqlite3", "cotacao.db")
	if err != nil {
		fmt.Println("Erro ao abrir o banco de dados:", err)
		return
	}
	defer db.Close()

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS cotacoes (id INTEGER PRIMARY KEY AUTOINCREMENT, valor REAL, timestamp DATETIME)")
	if err != nil {
		fmt.Println("Erro ao criar a tabela no banco de dados:", err)
		return
	}

	http.HandleFunc("/cotacao", func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(r.Context(), 200*time.Millisecond)
		defer cancel()

		cotacao, err := getCotacao(ctx)
		if err != nil {
			http.Error(w, "Erro ao obter a cotação", http.StatusInternalServerError)
			return
		}

		ctx, cancel = context.WithTimeout(ctx, 10*time.Millisecond)
		defer cancel()

		_, err = registrarCotacao(ctx, db, cotacao)
		if err != nil {
			fmt.Println("Erro ao registrar a cotação no banco de dados:", err)
			http.Error(w, "Erro ao registrar a cotação", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(cotacao["USDBRL"])
	})

	fmt.Println("Servidor iniciado na porta 8080")
	http.ListenAndServe(":8080", nil)
}

func getCotacao(ctx context.Context) (map[string]interface{}, error) {
	resp, err := http.Get("https://economia.awesomeapi.com.br/json/last/USD-BRL")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result map[string]interface{}
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}

	return result, nil
}

func registrarCotacao(ctx context.Context, db *sql.DB, cotacao map[string]interface{}) (string, error) {
	usdbrl, ok := cotacao["USDBRL"].(map[string]interface{})

	if !ok {
		return "", fmt.Errorf("valor do câmbio não encontrado na resposta")
	}

	bid, ok := usdbrl["bid"].(string)

	if !ok {
		return "", fmt.Errorf("valor do câmbio não encontrado na resposta")
	}

	_, err := db.ExecContext(ctx, "INSERT INTO cotacoes (valor, timestamp) VALUES (?, ?)", bid, time.Now())
	if err != nil {
		return "", err
	}

	return bid, nil
}
