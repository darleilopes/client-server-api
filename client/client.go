package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 300*time.Millisecond)
	defer cancel()

	client := &http.Client{
		Timeout: 300 * time.Millisecond,
	}

	req, err := http.NewRequest("GET", "http://localhost:8080/cotacao", nil)
	if err != nil {
		fmt.Println("Erro ao criar o request:", err)
		return
	}
	req = req.WithContext(ctx)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Erro ao realizar a requisição HTTP:", err)
		return
	}
	defer resp.Body.Close()

	// Leia o corpo da resposta
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Erro ao ler a resposta HTTP:", err)
		return
	}

	var result map[string]interface{}
	if err := json.Unmarshal(data, &result); err != nil {
		fmt.Println("Erro ao decodificar o JSON:", err)
		return
	}

	if bid, ok := result["bid"].(string); !ok {
		fmt.Println("Valor do câmbio não encontrado na resposta")
		return
	} else {
		// Salve o valor do câmbio em um arquivo
		if err := os.WriteFile("cotacao.txt", []byte(fmt.Sprintf("Dólar: %s\n", bid)), 0644); err != nil {
			fmt.Println("Erro ao salvar o valor do câmbio em um arquivo:", err)
			return
		}
	}

	fmt.Println("Valor do câmbio salvo com sucesso em cotacao.txt")
}
